# RateMyDarkness
```
“We can easily forgive a child who is afraid of the dark; 
but what about users, who try to upload too dark images?”
```
*~Plato, inaccurate quote*

## Description
This IntelliJ Idea project's purpose is to filter out too dark images uploaded by 
users. Verdict is based on a brightness cut-off point determined 
using sample images batch provided by business in advance. 

__It manages to properly classify all pictures provided in a sample.__

## Structure
Project consists of 3 classes.

### RateMyDarkness.scala 
Main class of application. Besides *main()* method it also contains helping 
methods for operating on files and *calculateCutOff()* method which calculates
the cut-off point mentioned in description above. The cut-off border is the point
between brightness of the darkest of bright images provided and the average 
brightness of the dark ones.

In *main* and *calculateCutOff* parallelism is implemented to make whole
solution more suitable for large amounts of pictures.

### ImageHandle.scala
This class contains:  

*measure()* method which measures the general brightness of an image by 
calculating brightness of each pixel in RGBstandard and scaling score to
 the maximum of 100.  
 
*label()* method which assigns brightness attribute to the image file.  

*readLabel()* method which allows reading file's metadata. Only used in
auxiliary class MoveBack

### MoveBack.scala
This class is made as a helper in debugging and is left in repository in
case of further debugging or future development.

### Directories
There are 4 subfolders in src directory:  
* in/ for input files  
* out/ where classified pictures are moved  
* bright/ and dark/ for samples used to calculate cut-off point  

