import java.io.File

import RateMyDarkness.{getListOfFiles, moveFile}

object
MoveBack {
    def main(args: Array[String]): Unit = {
        val in_path = getClass.getResource("/in")
        val out_path = getClass.getResource("/out")
        val okFileExtensions = List("jpg", "png")
        val in_pics = getListOfFiles(new File(out_path.getPath), okFileExtensions)
    
        for(element<-in_pics){
            println(element.getName + " labels: " + ImageHandle.readLabel(element.getAbsolutePath))
            moveFile(element.getAbsolutePath, in_path.getPath + "/" + element.getName)
        }
    }
}