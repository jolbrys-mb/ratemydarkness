import java.io.File
import java.net.URL
import java.nio.file.{FileSystems, Files, StandardCopyOption}
import java.util.logging.Logger

import scala.collection.parallel.CollectionConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object RateMyDarkness {
    
    val LOGGER = Logger.getLogger(classOf[Nothing].getName)
    val okFileExtensions = List("jpg", "png")
    
    def main(args: Array[String]): Unit = {
        var cut_off: Int = calculateCutOff()
        LOGGER.info("cut off point  set at " + cut_off + "/100.")
        var in_path: URL = null
        var out_path: URL = null

        try {
            in_path = getClass.getResource("/in")
            out_path = getClass.getResource("/out")
        }
        catch{
            case x: Exception => {
                LOGGER.warning("something is messed up in directories")
            }
        }
        val in_pics = getListOfFiles(new File(in_path.getPath), okFileExtensions)
    
        for(element <- in_pics.par){
            var measurement = ImageHandle.measure(element.getAbsolutePath)
            if(measurement <= cut_off) ImageHandle.label(element.getAbsolutePath, measurement, "dark")
            else ImageHandle.label(element.getAbsolutePath, measurement, "bright")
            moveFile(element.getAbsolutePath, out_path.getPath + "/" + element.getName)
            LOGGER.info("moving file: " + element.getName)
        }
    }
    
    def getListOfFiles(dir: File, extensions: List[String]): List[File] = {
        dir.listFiles.filter(_.isFile).toList.filter { file =>
            extensions.exists(file.getName.endsWith(_))
        }
    }
    
    def moveFile(source: String, dest: String): Unit ={
        val fileSys = FileSystems.getDefault
        val d1 = fileSys.getPath(source)
        val d2 = fileSys.getPath(new File(dest).toString)
        Files.move(d1, d2, StandardCopyOption.REPLACE_EXISTING)
    }
    
    def calculateCutOff(): Int = {
        var cut_off_calculated: Int = 20
        val bright_path = getClass.getResource("/bright")
        val dark_path = getClass.getResource("/dark")
        val bright_pics = getListOfFiles(new File(bright_path.getPath), okFileExtensions)
        val dark_pics = getListOfFiles(new File(dark_path.getPath), okFileExtensions)
        var low_b = ImageHandle.measure(bright_pics.head.getAbsolutePath)
        var d_count: Int = 0
        var avg_d: Double = 0.00
    
        val f1 = Future {
           for (element <- bright_pics) if (ImageHandle.measure(element.getAbsolutePath) < low_b) low_b = ImageHandle.measure(element.getAbsolutePath)
       }
        val f2 = Future {
            for (element <- dark_pics) {
                avg_d += ImageHandle.measure(element.getAbsolutePath)
                d_count += 1
            }
            avg_d = avg_d / d_count
        }

        val futures = for {
            r1 <- f1
            r2 <- f2
        } yield (r1, r2)
    
        futures.onComplete {
            case Success(x) => cut_off_calculated = Math.round((low_b + avg_d)/2).toInt
            case Failure(e) => e.printStackTrace
        }
        LOGGER.info("calculating cut off point...")
        while(!futures.isCompleted) Thread.sleep(50)
        
        return cut_off_calculated
    }
}
