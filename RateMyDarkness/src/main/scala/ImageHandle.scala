import java.awt.image.BufferedImage
import java.io.{File, FileNotFoundException}
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.nio.file.attribute.UserDefinedFileAttributeView
import java.nio.file.{FileSystems, Files}
import java.util.logging.Logger

import javax.imageio.ImageIO

object ImageHandle {
    val LOGGER = Logger.getLogger(classOf[Nothing].getName)
    
    def measure(filepath: String): Int ={
        val myFile = new File(filepath)
        val img: BufferedImage = ImageIO.read(myFile)
        
        var width: Int = 0
        var height: Int = 0
        var count: Int = 0
        var avg: Double = 0.00
        
        try {
            width = img.getWidth()
            height = img.getHeight()
        }
        catch{
            case x: FileNotFoundException => {
                LOGGER.warning("something is messed up with the images")
            }
        }
        
        for( x <- 0 until width) {
            for (y <- 0 until height) {
                var pixelCol: Int = img.getRGB(x, y)
                var r: Int = (pixelCol >>> 16) & 0xff
                var g: Int = (pixelCol >>> 8) & 0xff
                var b: Int = pixelCol & 0xff
                var lum: Double = (0.2126 * r) + (0.7152 * g) + (0.0722 * b)
    
                avg += lum
                count += 1
            }
        }
        avg = Math.round(avg/count/255*100)
        return avg.toInt
    }
  
    def label(file: String, score: Int, lum: String): Unit ={
        val fileSys = FileSystems.getDefault
        val path = fileSys.getPath(file)
        val view = Files.getFileAttributeView(path, classOf[UserDefinedFileAttributeView])
        view.write("user.score", Charset.defaultCharset.encode(score.toString))
        view.write("user.brightness", Charset.defaultCharset.encode(lum))
    }
    
    def readLabel(file: String): (String, String) ={
        val fileSys = FileSystems.getDefault
        val path = fileSys.getPath(file)
        val view = Files.getFileAttributeView(path, classOf[UserDefinedFileAttributeView])
        val name1 = "user.brightness"
        val name2 = "user.score"
        val buf1 = ByteBuffer.allocate(view.size(name1))
        val buf2 = ByteBuffer.allocate(view.size(name2))
        
        view.read(name1, buf1)
        buf1.flip
        view.read(name2, buf2)
        buf2.flip
        
        val value1 = Charset.defaultCharset.decode(buf1).toString
        val value2 = Charset.defaultCharset.decode(buf2).toString
        return (value1, value2)
    }
}